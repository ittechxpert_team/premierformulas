<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'premierformulas' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '23Mx6DBCxEZdVf#~Pzj!9p;+_,oSI:bkhlw2tBpxt{Q.:/0`HZz`bUIrym1m*:+u' );
define( 'SECURE_AUTH_KEY',  'UeotPPjlbZ?*GpJ58Y0h1r~0;Nli_nXr<Ie]kH.w3!;/[?cVfgJE{Eu`R:Ei&qsD' );
define( 'LOGGED_IN_KEY',    '{0#w57d}5I/$xS+l3c6!5~i3-yCF?|,x4YUCeUwzP%F=hkTNK4I;?005P(Ncq3{9' );
define( 'NONCE_KEY',        '9*xf@(19qzOH#YP|K_I*a=Tm`jx9mT 9d86.1k.l<xP1F0d!d9?BJ^mNz6vY^@o^' );
define( 'AUTH_SALT',        'wmR.bsha+g{JU;v8HrLkcT,e5Jj7)tH,dB6(]$*=V,]Hg6$5.SE+{&K)Ry5f3$F&' );
define( 'SECURE_AUTH_SALT', '`$V}V3GVO7F*%}lsJv&TV~{WFubJnM4>^u)ihD|uncXA&IEpvG8R#wva[#?g~i~S' );
define( 'LOGGED_IN_SALT',   'tXVI%Jg6IS^k%9?aFzK[kNDj{T^4.83WNYgEf;z!a_z[V~m)*{`|oq8y9Ka#-|af' );
define( 'NONCE_SALT',       '6<f~WK;CzHMoxRa@GAC]#Jb3f`k>wYoKlK}zv-/zxJM0?gz?5})+%n!!Hec8ZwIF' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
