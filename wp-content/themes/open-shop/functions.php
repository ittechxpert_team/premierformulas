<?php

/**
 * open shop functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Open Shop
 * @since 1.0.0
 */
/**
 * Theme functions and definitions
 */
if (!function_exists('open_shop_setup')) :
	define('OPEN_SHOP_THEME_VERSION', '1.4.3');
	define('OPEN_SHOP_THEME_DIR', get_template_directory() . '/');
	define('OPEN_SHOP_THEME_URI', get_template_directory_uri() . '/');
	define('OPEN_SHOP_THEME_SETTINGS', 'open-shop-settings');
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_open_shop_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function open_shop_setup()
	{
		/*
		 * Make theme available for translation.
		 */
		load_theme_textdomain('open-shop', get_template_directory() . '/languages');
		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support('title-tag');
		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support('post-thumbnails');
		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support('html5', array(
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		));
		add_theme_support('woocommerce');

		// Add support for Block Styles.
		add_theme_support('wp-block-styles');

		// Add support for full and wide align images.
		add_theme_support('align-wide');

		// Add support for editor styles.
		add_theme_support('editor-styles');

		// Enqueue editor styles.
		add_editor_style('style-editor.css');
		// Add support for responsive embedded content.
		add_theme_support('responsive-embeds');
		// Add theme support for selective refresh for widgets.
		add_theme_support('customize-selective-refresh-widgets');

		//Remove theme support for widget block editor 
		/**
		 * Add support for core custom logo.
		 */
		add_theme_support('custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		));
		// Add support for Custom Header.
		add_theme_support(
			'custom-header',

			apply_filters(
				'open_shop_custom_header_args',
				array(
					'default-image' => 'thntc_tim',
					'flex-height'   => true,
					'header-text'   => false,
					'video'          => false,
				)


			)
		);

		// Add support for Custom Background.
		if (get_theme_mod('open_shop_color_scheme') == 'opn-dark') {
			$args = array(
				'default-color' => '2f2f2f',
			);
		} else {
			$args = array(
				'default-color' => 'f1f1f1',
			);
		}
		add_theme_support('custom-background', $args);

		$GLOBALS['content_width'] = apply_filters('open_shop_content_width', 640);
		add_theme_support('woocommerce', array(
			'thumbnail_image_width' => 320,
		));


		// Recommend plugins
		add_theme_support('recommend-plugins', array(

			'hunk-companion' => array(
				'name' => esc_html__('Hunk Companion (Highly Recommended)', 'open-shop'),
				'img' => 'icon-128x128.png',
				'active_filename' => 'hunk-companion/hunk-companion.php',
			),

			'th-advance-product-search' => array(
				'name' => esc_html__('TH Advance Product Search', 'open-shop'),
				'img' => 'icon-128x128.gif',
				'active_filename' => 'th-advance-product-search/th-advance-product-search.php',
			),

			'th-all-in-one-woo-cart' => array(
				'name' => esc_html__('Th All In One Woo Cart', 'open-shop'),
				'img' => 'icon-128x128.png',
				'active_filename' => 'th-all-in-one-woo-cart/th-all-in-one-woo-cart.php',
			),

			'th-product-compare' => array(
				'name' => esc_html__('Th Product Compare', 'open-shop'),
				'img' => 'icon-128x128.png',
				'active_filename' => 'th-product-compare/th-product-compare.php',
			),

			'th-variation-swatches' => array(
				'name' => esc_html__('TH Variation Swatches', 'open-shop'),
				'img' => 'icon-128x128.gif',
				'active_filename' => 'th-variation-swatches/th-variation-swatches.php',
			),
			'lead-form-builder' => array(
				'name' => esc_html__('Lead Form Builder', 'open-shop'),
				'img' => 'icon-128x128.png',
				'active_filename' => 'lead-form-builder/lead-form-builder.php',
			),
			'wp-popup-builder' => array(
				'name' => esc_html__('WP Popup Builder – Popup Forms & Newsletter', 'open-shop'),
				'img' => 'icon-128x128.png',
				'active_filename' => 'wp-popup-builder/wp-popup-builder.php',
			),

			'unlimited-blocks' => array(
				'name' => esc_html__('Unlimited blocks For Gutenberg', 'open-shop'),
				'img' => 'icon-128x128.png',
				'active_filename' => 'unlimited-blocks/unlimited-blocks.php',
			),
			'woocommerce' => array(
				'name' => esc_html__('Woocommerce', 'open-shop'),
				'img' => 'icon-128x128.png',
				'active_filename' => 'woocommerce/woocommerce.php',
			),

			'yith-woocommerce-wishlist' => array(
				'name' => esc_html__('YITH WooCommerce Wishlist', 'open-shop'),
				'img' => 'icon-128x128.jpg',
				'active_filename' => 'yith-woocommerce-wishlist/init.php',
			),

			'themehunk-megamenu-plus' => array(
				'name' => esc_html__('ThemeHunk Megamenu – Menu builder', 'open-shop'),
				'img' => 'icon-128x128.png',
				'active_filename' => 'themehunk-megamenu-plus/themehunk-megamenu.php',
			),


		));

		// Import Data Content plugins
		add_theme_support('import-demo-content', array(

			'hunk-companion' => array(
				'name' => esc_html__('Hunk Companion', 'open-shop'),
				'img' => 'icon-128x128.png',
				'active_filename' => 'hunk-companion/hunk-companion.php',
			),

			'one-click-demo-import' => array(
				'name' => esc_html__('One Click Demo Import', 'open-shop'),
				'img' => 'icon-128x128.png',
				'active_filename' => 'one-click-demo-import/one-click-demo-import.php',
			),
			'woocommerce' => array(
				'name' => esc_html__('Woocommerce', 'open-shop'),
				'img' => 'icon-128x128.png',
				'active_filename' => 'woocommerce/woocommerce.php',
			),
			'th-advance-product-search' => array(
				'name' => esc_html__('TH Advance Product Search', 'open-shop'),
				'img' => 'icon-128x128.gif',
				'active_filename' => 'th-advance-product-search/th-advance-product-search.php',
			),

			'th-all-in-one-woo-cart' => array(
				'name' => esc_html__('TH All In One Woo Cart', 'open-shop'),
				'img' => 'icon-128x128.png',
				'active_filename' => 'th-all-in-one-woo-cart/th-all-in-one-woo-cart.php',
			),


		));



		// Useful plugins
		add_theme_support('useful-plugins', array(
			'themehunk-megamenu-plus' => array(
				'name' => esc_html__('Megamenu plugin from Themehunk.', 'open-shop'),
				'active_filename' => 'themehunk-megamenu-plus/themehunk-megamenu.php',
			),
		));
	}
endif;
add_action('after_setup_theme', 'open_shop_setup');
/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 */
/**
 * Register widget area.
 */
function open_shop_widgets_init()
{
	register_sidebar(array(
		'name'          => esc_html__('Primary Sidebar', 'open-shop'),
		'id'            => 'sidebar-1',
		'description'   => esc_html__('Add widgets here to appear in your primary sidebar.', 'open-shop'),
		'before_widget' => '<div id="%1$s" class="widget %2$s"><div class="open-shop-widget-content">',
		'after_widget'  => '</div></div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	));
	register_sidebar(array(
		'name'          => esc_html__('Above Header First Widget', 'open-shop'),
		'id'            => 'top-header-widget-col1',
		'description'   => esc_html__('Add widgets here to appear in top header.', 'open-shop'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	));
	register_sidebar(array(
		'name'          => esc_html__('Above Header Second Widget', 'open-shop'),
		'id'            => 'top-header-widget-col2',
		'description'   => esc_html__('Add widgets here to appear in top header.', 'open-shop'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	));
	register_sidebar(array(
		'name'          => esc_html__('Above Header Third Widget', 'open-shop'),
		'id'            => 'top-header-widget-col3',
		'description'   => esc_html__('Add widgets here to appear in top header.', 'open-shop'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	));

	register_sidebar(array(
		'name'          => esc_html__('Main Header Widget', 'open-shop'),
		'id'            => 'main-header-widget',
		'description'   => esc_html__('Add widgets here to appear in main header.', 'open-shop'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	));
	register_sidebar(array(
		'name'          => esc_html__('Footer Top First Widget', 'open-shop'),
		'id'            => 'footer-top-first',
		'description'   => esc_html__('Add widgets here to appear in top footer.', 'open-shop'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	));
	register_sidebar(array(
		'name'          => esc_html__('Footer Top Second Widget', 'open-shop'),
		'id'            => 'footer-top-second',
		'description'   => esc_html__('Add widgets here to appear in top footer.', 'open-shop'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	));
	register_sidebar(array(
		'name'          => esc_html__('Footer Top Third Widget', 'open-shop'),
		'id'            => 'footer-top-third',
		'description'   => esc_html__('Add widgets here to appear in top footer.', 'open-shop'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	));
	register_sidebar(array(
		'name'          => esc_html__('Footer Below First Widget', 'open-shop'),
		'id'            => 'footer-below-first',
		'description'   => esc_html__('Add widgets here to appear in top footer.', 'open-shop'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	));
	register_sidebar(array(
		'name'          => esc_html__('Footer Below Second Widget', 'open-shop'),
		'id'            => 'footer-below-second',
		'description'   => esc_html__('Add widgets here to appear in top footer.', 'open-shop'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	));
	register_sidebar(array(
		'name'          => esc_html__('Footer Below Third Widget', 'open-shop'),
		'id'            => 'footer-below-third',
		'description'   => esc_html__('Add widgets here to appear in top footer.', 'open-shop'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	));
	for ($i = 1; $i <= 4; $i++) {
		register_sidebar(array(
			'name'          => sprintf(esc_html__('Footer Widget Area %d', 'open-shop'), $i),
			'id'            => 'footer-' . $i,
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
		));
	}
}
add_action('widgets_init', 'open_shop_widgets_init');
/**
 * Enqueue scripts and styles.
 */
function open_shop_scripts()
{

	// enqueue css
	$min = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? '' : '.min';

	wp_enqueue_style('font-awesome', OPEN_SHOP_THEME_URI . '/third-party/fonts/font-awesome/css/font-awesome.css', '', OPEN_SHOP_THEME_VERSION);

	wp_enqueue_style('th-icon', OPEN_SHOP_THEME_URI . '/third-party/fonts/th-icon/style.css', '', OPEN_SHOP_THEME_VERSION);

	wp_enqueue_style('animate', OPEN_SHOP_THEME_URI . '/css/animate.css', '', OPEN_SHOP_THEME_VERSION);
	wp_enqueue_style('open-shop-menu', OPEN_SHOP_THEME_URI . '/css/open-shop-menu.css', '', OPEN_SHOP_THEME_VERSION);

	if ((bool)get_theme_mod('open_shop_rtl') == true || in_array('rtl', get_body_class())) {
		wp_enqueue_style('open-shop-rtl-style', OPEN_SHOP_THEME_URI . 'css/rtl.css', '', OPEN_SHOP_THEME_VERSION);
	} else {
		wp_enqueue_style('open-shop-main-style', OPEN_SHOP_THEME_URI . 'css/style.css', '', OPEN_SHOP_THEME_VERSION);
	}
	wp_enqueue_style('open-shop-style', get_stylesheet_uri(), array(), OPEN_SHOP_THEME_VERSION);
	wp_add_inline_style('open-shop-style', open_shop_custom_style());
	//enqueue js
	//wp_enqueue_script("jquery-effects-core",array( 'jquery' ));
	wp_enqueue_script('jquery-ui-autocomplete', array('jquery'), '', true);
	wp_enqueue_script('imagesloaded');
	wp_enqueue_script('open-shop-pro-menu-js', OPEN_SHOP_THEME_URI . '/js/open-shop-menu.js', array('jquery'), '1.0.0', true);
	wp_enqueue_script('open-shop-accordian-menu-js', OPEN_SHOP_THEME_URI . '/js/open-shop-accordian-menu.js', array('jquery'), OPEN_SHOP_THEME_VERSION, true);
	wp_enqueue_script('open-shop-custom-js', OPEN_SHOP_THEME_URI . '/js/open-shop-custom.js', array('jquery'), OPEN_SHOP_THEME_VERSION, true);
	$openshoplocalize = array(
		'open_shop_top_slider_optn' => (bool) get_theme_mod('open_shop_top_slider_optn', false),
		'open_shop_move_to_top_optn' => (bool) get_theme_mod('open_shop_move_to_top', false),
	);
	wp_localize_script('open-shop-custom-js', 'open_shop',  $openshoplocalize);
	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
}
add_action('wp_enqueue_scripts', 'open_shop_scripts');


if (!function_exists('wp_body_open')) {

	/**
	 * Shim for wp_body_open, ensuring backward compatibility with versions of WordPress older than 5.2.
	 */
	function wp_body_open()
	{
		do_action('wp_body_open');
	}
}

/********************************************************/
// Adding Dashicons in WordPress Front-end
/********************************************************/
add_action('wp_enqueue_scripts', 'open_shop_load_dashicons_front_end');
function open_shop_load_dashicons_front_end()
{
	wp_enqueue_style('dashicons');
}
/**
 * Load init.
 */
require_once trailingslashit(OPEN_SHOP_THEME_DIR) . 'inc/init.php';
//custom function conditional check for blog page
function open_shop_is_blog()
{
	return (is_archive() || is_author() || is_category() || is_home() || is_single() || is_tag()) && 'post' == get_post_type();
}

if (!empty(get_option('theme_mods_openshop-pro'))) {
	function open_shop_theme_switch()
	{
		update_option('theme_mods_openshop-pro', get_option('theme_mods_open-shop'));
	}
	add_action('switch_theme', 'open_shop_theme_switch');
}
/**
 * Auto Complete all WooCommerce orders.
 */
add_action('woocommerce_thankyou', 'custom_woocommerce_auto_complete_order');
function custom_woocommerce_auto_complete_order($order_id)
{
	if (!$order_id) {
		return;
	}

	$order = wc_get_order($order_id);
	$order->update_status('completed');
}


function addAdminPageContent()
{
	add_menu_page('Theme Setting', 'Theme Setting', 'manage_options', 'cadastro', 'crudAdminPage', 'dashicons-edit-page', 6);

	add_submenu_page('cadastro',  'Setting', 'Setting',  'manage_options', 'setting', 'crudAdmiSubnPage');
}

add_action('admin_menu', 'addAdminPageContent');


function crudAdminPage()

{
	if (isset($_POST['save_theme_options'])) {
		update_option('color_primary', $_POST['color_primary']);
		update_option('color_secondary', $_POST['color_secondary']);
	}
	$color_primary = get_option('color_primary');
	$color_secondary = get_option('color_secondary');
?>
	This is a Theme Setting page .
	<div class="container mt-3">
		<h2>Stacked form</h2>
		<form method="post">
			<label for="text">Theme Color Primary :</label><br>
			<input type="text" class="input" id="color_primary" name="color_primary" value="<?= $color_primary; ?>"><br>
			<label for="color_secondary">THeme Color Secondary:</label><br>
			<input type="text" class="input" id="color_secondary" name="color_secondary" value="<?= $color_secondary; ?>"><br><br>
			<input type="submit" name="save_theme_options" value="Submit">
		</form>
	</div>
<?php
}


function crudAdmiSubnPage()
{
	global $wpdb;

	if (isset($_POST['save_theme_options'])) {
		// print_r($_POST);

		$data = array(
			'id' => $_POST['id'],
			'fname' => $_POST['fname'],
			'lname' => $_POST['lname'],
			'email' => $_POST['email'],
			'pass' => $_POST['pass'],
		);

		$table_name = 'wp_theme_setting';
		$result = $wpdb->insert($table_name, $data, $format = NULL);

		if ($result == 1) {
			echo "yes";
		} else {
			echo "no";
		}
	};
	// $fname = $_POST['fname'];
	// $lname = $_POST['lname'];
	// $email = $_POST['email'];
	// $pass = $_POST['pass'];
?>

	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
	</head>
	This is a Theme Setting page 2 .
	<div class="container mt-3">
		<form method="post">
			<label for="fname">First name:</label><br>
			<input type="text" id="fname" name="fname" value="<?php #echo $fname;
																?>"><br>
			<label for="lname">Last name:</label><br>
			<input type="text" id="lname" name="lname" value="<?php #echo $lname; 
																?>"><br>
			<label for="email">Email name:</label><br>
			<input type="email" id="email" name="email" value="<?php #echo $email; 
																?>"><br>
			<label for="pass">Pass Word:</label><br>
			<input type="pass" id="pass" name="pass" value="<?php #echo  $pass;  
															?>"><br><br>
			<input type="submit" name="save_theme_options" value="Submit">
		</form>
	</div>
	<div class="mt-5">
		<?php
		global $wpdb;
		$result = $wpdb->get_results("select * from wp_theme_setting");
		?>
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Firstname</th>
					<th>Lastname</th>
					<th>Email</th>
					<th>Pass Word</th>
					<th>Edit Form</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($result as $table_name) {
				?>
					<tr>
						<th><?php echo $table_name->id; ?></th>
						<td><?php echo $table_name->fname; ?></td>
						<td><?php echo $table_name->lname; ?></td>
						<td><?php echo $table_name->email; ?></td>
						<td><?php echo $table_name->pass; ?></td>
						<td><input type="submit" value="Edit" name="update"></td>
						<td><input type="submit" value="Delete" name="delete"></td>
					</tr>
				<?php
				}
				?>
			</tbody>
		</table>
	</div>
<?php
}
?>